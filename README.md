# XorCrypt: Simple XOR Encryption

A simple Python3 script that implements XOR encryption. See [**xorcrypt.py**](./src/xorcrypt.py) for details.

Due to the nature of XOR, a file can be encrypted and decrypted using the same two-way function assuming the same key is provided.

### Disclaimer

XOR encryption is **exploitable** and is in no way a secure encryption system. This project exists solely for demonstrative purposes and should not be used in cryptographically secure solutions.

### Setup

This project requires the [Python interpreter](https://python.org). The minimum supported Python version is `3.10`.
```shell
$ python3 --version
Python 3.11.4
```

To run the script, first clone the project from the [GitLab repository](https://gitlab.com/lunardev/simple-xor-encryption):
```shell
$ git clone git@gitlab.com:lunardev/xorcrypt.git
$ cd xorcrypt/src
$ python3 xorcrypt.py --help
```

### CLI Flags

```
usage: xorcrypt.py [-h] [-i] [-k KEY] [-e EXT] [file]

positional arguments:
  file                file to be encrypted or decrypted (default: read stdin)

options:
  -h, --help          show this help message and exit
  -i, --intermittent  utilise intermittent encryption techniques (default: False)
  -k KEY, --key KEY   file containing the encryption key used to encrypt data (default: "xor.key")
  -e EXT, --ext EXT   file extension to append to the encrypted file name (default: ".out")
```

### Examples

Encrypt the contents of a new file.
```shell
$ # Create an example file.
$ echo "hello, world!" > file.txt
$ # Encrypt the data with a keyfile.
$ ./xorcrypt.py -k=keyfile file.txt
Saved key to keyfile: /home/user/.../keyfile
Saved data to file: /home/user/.../file.txt.out
$ # Output encrypted file contents.
$ cat file.txt.out | hexdump -C
00000000  81 51 04 9a bc 80 2e b3  bd 20 47 7c ce e4        |.Q....... G|..|
0000000e
```

Accept data piped from another command:
```bash
$ # Get random data for example purposes.
$ data=$(dd if=/dev/urandom bs=32 count=1 status=none | b2sum -b | sed 's/ .*//')
$ echo $data | ./xorcrypt.py
Saved data to file: /home/user/.../xor_4b4c1f2d.out
```

### Intermittent Encryption

The `xorcrypt` script implements "intermittent encryption"; where data is split into chunks that alternate between being encrypted or left in plaintext.

The general purpose for this algorithm is to reduce encryption time while still obscuring some data. However, the encryption implementation of the project does not benefit from a notable time improvement and only exits as a proof-of-concept.

This behaviour can be enabled with the `--intermittent` flag.

![Example of intermittent encryption](./assets/intermittent_encryption.png)

### License

This project is licensed under the [MIT license](./LICENSE).
