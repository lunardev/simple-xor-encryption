#!/usr/bin/env python3

"""XorCrypt: A simple Python3 script that implements XOR encryption."""

import sys
from argparse import ArgumentParser
from dataclasses import dataclass
from itertools import cycle
from pathlib import Path
from secrets import SystemRandom
from typing import Iterator

__author__ = "T. Rodriguez"
__license__ = "MIT License"
__version__ = "1.0.0"
__repository__ = "https://gitlab.com/lunardev/xorcrypt"

if sys.version_info < (3, 10, 0):
    version = ".".join(str(i) for i in sys.version_info[:3])
    exception = ImportError if __name__ != "__main__" else SystemExit
    raise exception(f"Python version {version} is not supported.")


@dataclass(frozen=True, kw_only=True)
class Args:
    """Dataclass of typed command line parameters."""

    intermittent: bool
    key: Path
    ext: str
    file: Path | None


def parse_args() -> Args:
    """
    Define the program's CLI parameters using argparse.

    -i, --intermittent:
        Utilise intermittent encryption techniques. Defaults to False.
    -k, --keyfile:
        File containing the encryption key used to encrypt data.
        Defaults to "xor.key".
    -e, --ext:
        File extension to append to the encrypted file name. Defaults
        to ".out".
    file:
        File to be encrypted or decrypted. Defaults to reading
        standard input.

    Returns:
        Args: Dataclass containing typed arguments.
    """
    parser = ArgumentParser(
        description="a simple python3 script that implements xor encryption"
    )
    parser.add_argument(
        "-i",
        "--intermittent",
        dest="intermittent",
        action="store_true",
        default=False,
        help="utilise intermittent encryption techniques (default: False)",
    )
    parser.add_argument(
        "-k",
        "--key",
        dest="key",
        default="xor.key",
        type=lambda x: Path(x).expanduser().resolve(),
        help='file containing the encryption key used to encrypt data (default: "xor.key")',
    )
    parser.add_argument(
        "-e",
        "--ext",
        dest="ext",
        default=".out",
        type=str,
        help='file extension to append to the encrypted file name (default: ".out")',
    )
    parser.add_argument(
        "file",
        nargs="?",
        default=None,
        type=_parse_file,
        help="file to be encrypted or decrypted (default: read stdin)",
    )
    return Args(**vars(parser.parse_args()))


def _parse_file(file: str) -> Path:
    """
    Parse a given file to ensure it is a valid path.

    Exits the program if the file can not be read. Otherwise returns a
    `pathlib.Path` object representing the file path.

    Args:
        file (str): System path to the file.

    Raises:
        IsADirectoryError: The provided path is a directory instead of
            a file.
        SystemExit: The provided file did not exist.

    Returns:
        Path: Expanded path of the file.
    """
    path = Path(file).expanduser()

    # Check that the provided path exists on the system.
    try:
        path = path.resolve(strict=True)
        if path.is_dir():
            raise IsADirectoryError
    except FileNotFoundError:
        print(f"File '{path!s}' does not exist", file=sys.stderr)
    except IsADirectoryError:
        print(f"Path '{path!s}' is a directory, not a file", file=sys.stderr)
    else:
        return path

    # Exit the process with exit code 1.
    raise SystemExit(1)


def read_from_keyfile(keyfile: Path) -> bytes:
    """
    Read encryption key from keyfile.

    Args:
        keyfile (Path): File path that the key is saved to.

    Raises:
        SystemExit: Failed to read the encryption key from the path.

    Returns:
        bytes: Encryption key bytestring.
    """
    try:
        with open(keyfile, mode="rb") as f:
            return f.read()
    except Exception as e:
        raise SystemExit(f"Failed to read from keyfile: {e}")


def write_to_keyfile(keyfile: Path, key: bytes) -> None:
    """
    Write encryption key to keyfile.

    Args:
        keyfile (Path): File path that the key will be saved to.
        key (bytes): Encryption key bytestring.

    Raises:
        SystemExit: Failed to write the encryption key to the path.
    """
    try:
        with open(keyfile, mode="wb") as f:
            f.write(key)
            print("Saved key to keyfile:", keyfile)
    except Exception as e:
        raise SystemExit(f"Failed to save to keyfile: {e}")


def parse_keyfile(path: Path) -> bytes:
    """
    Read key from path or generate a key if the file does not exist.

    Args:
        path (Path): File path containing the encryption key.

    Returns:
        bytes: Encryption key bytestring.
    """
    if path.is_file():
        return read_from_keyfile(path)
    # Generate a random key of 255 bytes.
    key = SYSRAND.randbytes(255)
    write_to_keyfile(path, key)
    return key


def intermittent_file_size(data: bytes) -> tuple[int, int]:
    """
    Determine step and skip increments depending on data size.

    The step determines how many bytes of data will be encrypted
    and skip determines how many bytes of data will be left unmodified.

    Args:
        data (bytes): Provided bytestring data.

    Returns:
        tuple[int, int]: Data step and skip.
    """
    size = len(data)
    kbyte = 1024
    if size < kbyte:  # < 1 Kb
        # 32 bytes
        step = skip = 32
    elif size < kbyte * 16:  # < 16 Kb
        # 32 bytes
        step = skip = 32
    else:  # >= 16 Kb
        # 96 bytes
        step = skip = 96

    return step, skip


def xor_data(
    data: bytes, key: bytes, /, *, step: int = 1, skip: int = 0
) -> Iterator[bytes]:
    """
    XOR each byte of data with the key bytes to encrypt and decrypt.

    Optionally supports intermittent encryption (IE) if the `step` or
    `skip` argument is provided.

    Args:
        data (bytes): Subject bytestring to be XORed.
        key (bytes): Key bytestring to XOR with the data.
        step (int, optional): Number of bytes to encrypt (IE).
        skip (int, optional): Number of bytes to skip (IE).

    Yields:
        Iterator[bytes]: Encrypted byte.
    """
    count = -1
    for i, j in zip(data, cycle(key)):
        count += 1
        if count == step + skip:
            count = 0
        elif count >= step:
            yield i.to_bytes(1, "big")
            continue
        yield (i ^ j).to_bytes(1, "big")


def main() -> None:
    """
    Entrypoint to the Python script.

    Parse CLI arguments and write encrypted data to file.
    """
    args = parse_args()

    if args.file is None:
        data = sys.stdin.buffer.read()
    else:
        data = args.file.read_bytes()

    key = parse_keyfile(args.key)

    step, skip = intermittent_file_size(data) if args.intermittent else (1, 0)
    output = b"".join(xor_data(data, key, step=step, skip=skip))

    if args.file is None:
        # Use random file name if not provided.
        path = Path.cwd() / f"xor_{SYSRAND.randbytes(4).hex()}{args.ext}"
    elif not args.file.suffixes or args.file.suffixes[-1] != args.ext:
        path = Path(f"{args.file!s}{args.ext}")
    else:
        path = args.file

    path.write_bytes(output)
    print("Saved data to file:", path)


if __name__ == "__main__":
    SYSRAND = SystemRandom()
    main()
